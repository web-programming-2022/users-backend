import { IsNotEmpty, MinLength, Min, IsPositive } from 'class-validator';

export class CreateProductDto {
  @MinLength(8)
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsNotEmpty()
  price: number;
}
